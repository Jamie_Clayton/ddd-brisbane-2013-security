﻿using System.Data.Entity;

namespace DemoService.Models
{
    public class EmployeeContext :DbContext
    {
        public EmployeeContext():base("Personnel"){}
        
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<EmployeeRole> EmployeeRoles { get; set; }
    }
}