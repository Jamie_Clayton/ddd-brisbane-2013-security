﻿using System.Collections.Generic;

namespace DemoService.Models
{
    public class EmployeeDto
    {
        public EmployeeDto()
        {
            Roles = new List<string>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
    }
}