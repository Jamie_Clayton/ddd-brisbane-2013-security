﻿using System.Collections.Generic;

namespace DemoService.Models
{
    public interface IDataService<T>
    {
        List<T> GetAll();
        T Get(int id);
        T Add(T item);
        T Update(T item);
        void Delete(int id);
    }
}