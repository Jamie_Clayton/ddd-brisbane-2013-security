﻿using System.Data.Entity.ModelConfiguration;

namespace DemoService.Models.Mapping
{
    public class EmployeeRoleMap : EntityTypeConfiguration<EmployeeRole>
    {
        public EmployeeRoleMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            ToTable("EmployeeRoles");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.EmployeeId).HasColumnName("EmployeeId");
            Property(t => t.RoleId).HasColumnName("RoleId");

            // Relationships
            HasRequired(t => t.Employee)
                .WithMany(t => t.EmployeeRoles)
                .HasForeignKey(d => d.EmployeeId);
            HasRequired(t => t.Role)
                .WithMany(t => t.EmployeeRoles)
                .HasForeignKey(d => d.RoleId);

        }
    }
}