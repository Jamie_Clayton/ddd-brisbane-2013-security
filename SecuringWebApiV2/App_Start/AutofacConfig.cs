﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using DemoService.Controllers;
using DemoService.Models;

namespace DemoService
{
    public class AutofacConfig
    {
        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetAssembly(typeof(PersonnelController)));
            builder.RegisterType<EmployeeDataService>().As<IDataService<EmployeeDto>>().SingleInstance();
            //builder.RegisterControllers(typeof (PersonnelController).Assembly);
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
         
    }
}